export class BufferGeometry {
        /**
         * This creates a new BufferGeometry. It also sets several properties to an default value.
         */
        constructor();

        static MaxIndex: number;


        //groups: {start: number, count: number, materialIndex?: number}[];
        clearDrawCalls(): void; // deprecated, use clearGroups()
        computeFaceNormals(): void; // deprecated
    }